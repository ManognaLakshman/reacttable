import "./index.css";
export const dep = [
    {

        DepName: "Finance",
        DeptHead: "Akarsha"
    },
    {

        DepName: "Marketing",
        DeptHead: "Thenmozhi"
    },
    {

        DepName: "Information Technology",
        DeptHead: "Ramanathan"
    },
    {

        DepName: "Human Resource Management",
        DeptHead: "Mythri"
    },
    {

        DepName: "Customer Support",
        DeptHead: "Srikesh"
    }
];
export const rows = [
    {
        ID: "01",
        Name: "Ram",
        Department: "Information Technology",
        Skill: "Business Analysis",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "4",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "02",
        Name: "Raj",
        Department: "Marketing",
        Skill: "Communication",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "3",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "03",
        Name: "Rani",
        Department: "Information Technology",
        Skill: "Business Analysis",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "2",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "04",
        Name: "Ramya",
        Department: "Information Technology",
        Skill: "Documentation and Specification",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "1",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "05",
        Name: "Meghana",
        Department: "Human Resource Management",
        Skill: "Employee Relations",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "4",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "06",
        Name: "Revathy",
        Department: "Information Technology",
        Skill: "Documentation and Specification",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "5",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "07",
        Name: "Aditya",
        Department: "Finance",
        Skill: "Financial Reporting",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "1",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "08",
        Name: "Saunak",
        Department: "Human Resource Management",
        Skill: "Onboarding",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "4",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "09",
        Name: "Krishna",
        Department: "Finance",
        Skill: "Financial Reporting",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "3",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "10",
        Name: "Shiva",
        Department: "Customer Support",
        Skill: "Time Management",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "5",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "11",
        Name: "Shivani",
        Department: "Human Resource Management",
        Skill: "Onboarding",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "3",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "12",
        Name: "Dev",
        Department: "Finance",
        Skill: "Financial Reporting",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "2",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "13",
        Name: "Lakshman",
        Department: "Finance",
        Skill: "Financial Reporting",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "4",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "14",
        Name: "Lucky",
        Department: "Human Resource Management",
        Skill: "Employee Relations",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "5",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "15",
        Name: "Manogna",
        Department: "Customer Support",
        Skill: "Time Management",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "4",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "16",
        Name: "Mythri",
        Department: "Customer Support",
        Skill: "Knowledge of the product",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "4",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "17",
        Name: "Ramesh",
        Department: "Customer Support",
        Skill: "Knowledge of the product",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "2",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "18",
        Name: "Supriya",
        Department: "Marketing",
        Skill: "Communication",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "3",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "19",
        Name: "Srikesh",
        Department: "Finance",
        Skill: "Commercial acumen",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "5",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "20",
        Name: "Nithin",
        Department: "Production",
        Skill: "Manufacturing Process",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "4",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "21",
        Name: "Naveen",
        Department: "Research & Development",
        Skill: "Attention to detail",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "4",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "22",
        Name: "Thenmozhi",
        Department: "Research & Development",
        Skill: "Technical skills",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "1",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "23",
        Name: "Ramanathan",
        Department: "Research & Development",
        Skill: "Technical skills",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "1",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "24",
        Name: "Akarsha",
        Department: "Production",
        Skill: "Manufacturing Process",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "3",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "25",
        Name: "Naveen",
        Department: "Research & Development",
        Skill: "Attention to detail",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "1",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "26",
        Name: "Arun",
        Department: "Purchasing",
        Skill: "Relationship building",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "4",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "27",
        Name: "Naren",
        Department: "Purchasing",
        Skill: "Negotiation",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "4",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "28",
        Name: "Nandana",
        Department: "Production",
        Skill: "Manufacturing Process",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "5",
        Salary: "10000",
        City: "Gotham",
        Country: "Akanda"
    },
    {
        ID: "29",
        Name: "Vidya",
        Department: "Purchasing",
        Skill: "Decision making",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "4",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    },
    {
        ID: "30",
        Name: "Varsha",
        Department: "Marketing",
        Skill: "Analytics",
        DOJ: "12-02-1995",
        Designation: "Manager",
        Grade: "4",
        Salary: "10000",
        City: "Gotham",
        Country: "Wakanda"
    }
];