import React from "react";
<<<<<<< HEAD
import DatePicker from "react-datepicker";
import './index.css';
//import moment from "moment";
import Dropdown from "react-dropdown";
import "react-dropdown/style.css";
import "react-datepicker/dist/react-datepicker.css";
import ReactTable from "react-table";
import "react-table/react-table.css";
import axios from "axios";
class Employee extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            emp_data: [],
            dep_data: {},
            isLoading: false,
            startDate: new Date()
        }
    }


    componentDidMount() {

        this.setState({ isLoading: true }, () => {

            axios.get("https://spring-employee.herokuapp.com/employees")
                .then(json => json.data._embedded.employees.map(result => (
                    {
                        ID: result.empid,
                        Name: result.empname,
                        Skill: result.skill,
                        Salary: result.salary,
                        Grade: result.grade,
                        City: result.city,
                        Country: result.country,
                        DOJ: result.doj,
                        DeptName: result.deptid.deptname,
                        Designation: result.designation,
                        dep_link: result._links.deptid.href


                    })))
                .then(newData => this.setState({ emp_data: newData, isLoading: false }))
                .catch(function (error) {
                    console.log(error);
                });

        });

        this.handleOriginal = this.handleOriginal.bind(this);
    }

    handleOriginal(var2) {
        if (!this.state.dep_data[var2]) {
            axios.get(var2)
                .then(json => {
                    const data1 = json.data;
                    this.setState({ dep_data: { ...this.state.dep_data, [var2]: data1 } });
                    console.log(json);

                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    }

    render() {
        let content;
        const { emp_data, isLoading } = this.state;
        if (isLoading) {
            content = <div>Loading...</div>;
        }
        else {

            content = <div>
                <ReactTable
                    data={emp_data}
                    filterable
                    defaultFilterMethod={(filter, row) =>
                        String(row[filter.id]) === filter.value
                    }
                    columns={[
                        {
                            Header: "ID",
                            accessor: "ID",
                            filterMethod: (filter, row) =>
                                String(row[filter.id]).startsWith(filter.value)
                        },
                        {
                            Header: "Name",
                            accessor: "Name",
                            filterMethod: (filter, row) =>
                                row[filter.id].startsWith(filter.value)
                        },

                        {
                            Header: "Skill",
                            accessor: "Skill",
                            filterMethod: (filter, row) =>
                                row[filter.id].startsWith(filter.value)
                        },
                        {
                            Header: "DOJ",
                            accessor: "DOJ",


                        },
                        {
                            Header: "Designation",
                            accessor: "Designation",
                            minWidth: 110,
                            filterMethod: (filter, row) =>
                                row[filter.id].startsWith(filter.value)
                        },
                        {
                            Header: "DeptName",
                            accessor: "DeptName",
                            filterMethod: (filter, row) =>
                                row[filter.id].startsWith(filter.value)
                        },
                        {
                            Header: "Grade",
                            accessor: "Grade",
                            filterMethod: (filter, row) =>
                                String(row[filter.id]).startsWith(filter.value)
                        },
                        {
                            Header: "Salary",
                            accessor: "Salary",
                            filterMethod: (filter, row) =>
                                String(row[filter.id]).startsWith(filter.value)
                        },
                        {
                            Header: "City",
                            accessor: "City",
                            filterMethod: (filter, row) =>
                                row[filter.id].startsWith(filter.value)
                        },
                        {
                            Header: "Country",
                            accessor: "Country",
                            filterMethod: (filter, row) =>
                                row[filter.id].startsWith(filter.value)
                        }
                    ]}
                    defaultSorted={[
                        {
                            id: "Name",
                            desc: true
                        }
                    ]}
                    defaultPageSize={10}
                    className="-striped -highlight"
                    getTdProps={() => {
                        return {
                            style: {
                                overflow: "visible"
                            }
                        };
                    }}

                    freezeWhenExpanded={true}
                    SubComponent={rows => {
                        const { dep_data } = this.state;
                        let var2 = "";
                        let var1 = rows.original.dep_link;
                        for (let i = 0; i < var1.length; i++) {
                            if (var1[i] !== '{') {
                                var2 += var1[i];
                            }
                            if (var1[i] === '{') {
                                break;
                            }
                        }
                        const dep = dep_data[var2];
                        //debugger;
                        if (dep) {
                            return (
                                <div className="Posts">
                                    <header>
                                        <ul>
                                            <li>Dep ID       : {dep.deptid}</li>
                                            <li>Dep Name     : {dep.deptname}</li>
                                            <li>Dep Head     : {dep.depthead.city}</li>
                                            <li>City         : {dep.depthead.country}</li>
                                            <li>Country      : {dep.depthead.designation}</li>
                                            <li>Designation  : {dep.depthead.doj}</li>
                                            <li>DOJ          : {dep.depthead.empname}</li>
                                            <li>Grade        : {dep.depthead.grade}</li>
                                            <li>Salary       : {dep.depthead.salary}</li>
                                            <li>Skill        : {dep.depthead.skill}</li>
                                        </ul>
                                    </header>

                                </div>
                            )
                        }
                        else {
                            return (<div className="Posts">
                                Loading...
                            </div>);
                        }
                    }


                    }
                    getTrProps={(state, rowInfo, column, instance) => {
                        return {
                            onClick: (e) => {
                                let var2 = "";
                                console.log(e);
                                console.log(rowInfo);

                                let var1 = rowInfo.row._original.dep_link;
                                for (let i = 0; i < var1.length; i++) {
                                    if (var1[i] !== '{') {
                                        var2 += var1[i];
                                    }
                                    if (var1[i] === '{') {
                                        break;
                                    }
                                }
                                //console.log(var2);
                                this.handleOriginal(var2);

                            }
                        }

                    }
                    }

                />
            </div >

        }
        return (
            <div>
                {content}
            </div >
        )
    }
=======
//import DatePicker from "react-datepicker";
import moment from "moment";
//import Dropdown from "react-dropdown";
import "react-dropdown/style.css";
//import "react-datepicker/dist/react-datepicker.css";
import ReactTable from "react-table";
import "react-table/react-table.css";
import "./index.css";
import axios from "axios";
class Employee extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      emp_data: [],
      isLoading: false,
      startDate: moment()
    };
  }

  componentDidMount() {
    this.setState({ isLoading: true }, () => {
      let a = 1;
      axios
        .get("https://spring-employee.herokuapp.com/employees")
        .then(json =>
          json.data._embedded.employees.map(result => ({
            EmpID: a++,
            Name: result.empname,
            Skill: result.skill,
            Salary: result.salary,
            Grade: result.grade,
            City: result.city,
            Country: result.country,
            DOJ: result.doj,
            Designation: result.designation
          }))
        )
        .then(newData => this.setState({ emp_data: newData, isLoading: false }))
        .catch(function(error) {
          console.log(error);
        });
    });

    //this.date_picker = this.date_picker.bind(this);
    //this.Dropdown = this.Dropdown.bind(this);
  }

  // Dropdown(cellInfo) {
  //   const data = [...this.state.emp_data];

  //   return (
  //     <Dropdown
  //       contentEditable
  //       onChange={e => {
  //         const data = [...this.state.emp_data];
  //         data[cellInfo.index][cellInfo.column.id] = e.value;
  //         this.setState({ emp_data: data });
  //       }}
  //       value={data[cellInfo.index][cellInfo.column.id]}
  //       placeholder="Select"
  //     />
  //   );
  // }

  // date_picker(cellInfo) {
  //   console.log();
  //   return (
  //     <DatePicker
  //       contentEditable
  //       selected={moment(
  //         this.state.emp_data[cellInfo.index][cellInfo.column.id],
  //         "YYYY-MM-DD"
  //       )}
  //       onChange={e => {
  //         const data = [...this.state.emp_data];
  //         data[cellInfo.index][cellInfo.column.id] = e;
  //         this.setState({ emp_data: data });
  //         console.log(data);
  //       }}
  //     />
  //   );
  // }

  render() {
    let content;
    const { emp_data, isLoading } = this.state;
    console.log(emp_data);
    if (isLoading) {
      content = <div>Loading...</div>;
    } else {
      content = (
        <div>
          <ReactTable
            data={emp_data}
            filterable
            defaultFilterMethod={(filter, row) =>
              String(row[filter.id]) === filter.value
            }
            columns={[
              {
                Header: "EmpID",
                accessor: "EmpID"
              },
              {
                Header: "Name",
                accessor: "Name",
                filterMethod: (filter, row) =>
                  row[filter.id].startsWith(filter.value)
              },
              {
                Header: "Skill",
                accessor: "Skill",
                filterMethod: (filter, row) =>
                  row[filter.id].startsWith(filter.value)
              },
              {
                Header: "DOJ",
                accessor: "DOJ",
                filterMethod: (filter, row) =>
                  row[filter.id].startsWith(filter.value)
              },
              {
                Header: "Designation",
                accessor: "Designation",
                //Cell: this.Dropdown,
                filterMethod: (filter, row) =>
                  row[filter.id].startsWith(filter.value)
              },

              {
                Header: "Grade",
                accessor: "Grade",
                filterMethod: (filter, row) =>
                  String(row[filter.id]).startsWith(filter.value)
              },
              {
                Header: "Salary",
                accessor: "Salary",
                filterMethod: (filter, row) =>
                  String(row[filter.id]).startsWith(filter.value)
              },
              {
                Header: "City",
                accessor: "City",
                filterMethod: (filter, row) =>
                  row[filter.id].startsWith(filter.value)
              },
              {
                Header: "Country",
                accessor: "Country",
                filterMethod: (filter, row) =>
                  row[filter.id].startsWith(filter.value)
              }
            ]}
            defaultSorted={[
              {
                id: "Name",
                desc: true
              }
            ]}
            defaultPageSize={5}
            className="-striped -highlight"
            getTdProps={() => {
              return {
                style: {
                  overflow: "visible"
                }
              };
            }}
          />
        </div>
      );
    }
    return <div>{content}</div>;
  }
>>>>>>> 0cecd2c203ea26602f7355155a16d6ad9c643d56
}
export default Employee;
