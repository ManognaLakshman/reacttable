import React from "react";
<<<<<<< HEAD
import axios from "axios";
import ReactTable from "react-table";
import "react-table/react-table.css";

class Department extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isLoading: false
        };

    }
    componentDidMount() {
        this.setState({ isLoading: true }, () => {
            axios.get("https://spring-employee.herokuapp.com/departments")
                .then(json => json.data._embedded.departments.map(result => (
                    {
                        DepID: result.deptid,
                        DeptName: result.deptname,
                        DeptHead: result.depthead.empname
                    }
                )))
                .then(newData => {
                    this.setState({ data: newData, isLoading: false })
                })
                .catch(function (error) {
                    console.log(error);
                })
        });
    }

    render() {
        let content;
        const { data, isLoading } = this.state;
        if (isLoading) {
            content = <div>Loading...</div>;
        }
        else {
            content = <div>

                <ReactTable
                    data={data}
                    filterable
                    defaultFilterMethod={(filter, row) =>
                        String(row[filter.id]) === filter.value
                    }
                    columns={[
                        {
                            Header: "DepID", accessor: "DepID",
                            filterMethod: (filter, row) =>
                                String(row[filter.id]).startsWith(filter.value)
                        },
                        {
                            Header: "DeptName", accessor: "DeptName",
                            filterMethod: (filter, row) =>
                                String(row[filter.id]).startsWith(filter.value)
                        },
                        {
                            Header: "DeptHead", accessor: "DeptHead",
                            filterMethod: (filter, row) =>
                                String(row[filter.id]).startsWith(filter.value)
                        }
                    ]}

                    defaultSorted={[
                        {
                            id: "DepName",
                            desc: false
                        }
                    ]
                    }
                    defaultPageSize={8}
                    className="-striped -highlight"
                />


            </div>
        }
        return (
            <div>
                {content}
            </div >
        )
    }
}

=======
import ReactTable from "react-table";
//import { rows2 } from "./Utils";
import "react-table/react-table.css";
import axios from "axios";
import "./index.css";

class Department extends React.Component {
  componentDidMount() {
    this.setState({ isLoading: true });

    axios
      .get("https://spring-employee.herokuapp.com/departments")
      .then(json =>
        json.data._embedded.departments.map(result => ({
          Department: result.deptname,
          DeptHead: result.depthead
        }))
      )
      .then(newData => this.setState({ dep_data: newData }))
      .catch(function(error) {
        console.log(error);
      });
  }
  constructor(props) {
    super(props);
    this.state = {
      dep_data: []
    };

    this.ClickAction = this.ClickAction.bind(this);
    this.performAction = this.performAction.bind(this);
  }
  ClickAction(cellInfo) {
    const data = [...this.state.dep_data];
    return (
      <div>
        <button
          contentEditable
          suppressContentEditableWarning
          onClick={this.performAction}
        >
          {data[cellInfo.index][cellInfo.column.id]}
        </button>
      </div>
    );
  }
  performAction() {}
  render() {
    const { dep_data } = this.state;

    return (
      <div>
        <ReactTable
          data={dep_data}
          columns={[
            {
              Header: "Department",
              accessor: "Department"
            },
            {
              Header: "DeptHead",
              accessor: "DeptHead",
              Cell: this.renderEditable
            }
          ]}
          defaultSorted={[
            {
              id: "DepartmentID",
              desc: true
            }
          ]}
          defaultPageSize={5}
          className="-striped -highlight"
        />
      </div>
    );
  }
}
>>>>>>> 0cecd2c203ea26602f7355155a16d6ad9c643d56
export default Department;
